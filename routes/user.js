'use strict';
import express from 'express';
const router = express.Router();

import * as controller from '../controllers/userController.js';

// database user insert
router.get('/insert', controller.insertUsers);

// database user select
router.get('/select', controller.selectUsers);

export default router;
