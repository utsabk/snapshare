'use strict';
import express from 'express';
const router = express.Router();
import * as controller from '../controllers/mediaController.js';


// get all images
router.get('/images', controller.selectMedia);

// respond to post and save file
router.post('/upload', controller.upload.array('mediafile', 3), controller.fileValidation)
// create thumbnail
router.post('/upload', controller.thumbnailResize);

// create medium image
router.post('/upload', controller.mediumResize);

// insert to database
router.post('/upload', controller.insert2DB);

// send ok response
router.post('/upload', controller.uploadMsg);

export default router;
