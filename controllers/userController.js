'use strict';
import {
  connect as connectDB,
  selectUser as selectUserDB,
  insertUser as insertUserDB,
} from '../db/database.js';

const insertUsers = (req, res) => {
  const data = ['testUser1', 'testEmail1', 'testPass1'];
  insertUserDB(
    data,
    connectDB(),
    (result, res) => {
      res.json(result);
    },
    res
  );
};

const selectUsers = (req, res) => {
  selectUserDB(
    connectDB(),
    (result, res) => {
      console.log('this is result inside callback', result);
      res.json(result);
    },
    res
  );
};

export { insertUsers, selectUsers };
