'use strict';
import multer from 'multer';
import path from 'path';
import fs from 'fs';

import resize from '../utils/resize.js';
import {
  connect as connectDB,
  insertMedia as insertMediaDB,
  selectMedia as selectMediaDB,
} from '../db/database.js';
//const upload = multer({ dest: './uploads/orginal/' });

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './uploads/orginal/');
  },
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + path.extname(file.originalname);

    cb(null, file.fieldname + '-' + uniqueSuffix);
  },
});

const upload = multer({ storage: storage });

const fileValidation = (req, res, next) => {
  const files = req.files;
  if (!files) {
    const error = new Error('Please upload a file');
    error.httpStatusCode = 400;
    return next(error);
  }
  next();
};

const thumbnailResize = (req, res, next) => {
  req.files.forEach(async (file) => {
    try {
      await resize(file.path, 300, `./uploads/thumbs/thumb_${file.filename}`);
    } catch (err) {
      console.log(err);
    }
  });

  next();
};

const mediumResize = (req, res, next) => {
  req.files.forEach(async (file) => {
    try {
      await resize(file.path, 640, `./uploads/medium/medium_${file.filename}`);
    } catch (err) {
      console.log(err);
    }
  });
  next();
};

const insert2DB = (req, res, next) => {
  req.files.forEach((file) => {
    const data = [
      req.body.category,
      req.body.title,
      req.body.details,
      `./uploads/thumbs/thumb_${file.filename}`,
      `./uploads/medium/medium_${file.filename}`,
      `./uploads/orginal/${file.filename}`,
      //req.user.uID
      'user  comes here',
    ];

    console.log('this is a data', data);

    try {
      insertMediaDB(data, connectDB(), next);
    } catch (err) {
      console.log('Error insert2DB :-', err);
    }
  });
};

const selectMedia = (req, res) => {
  selectMediaDB(
    connectDB(),
    (result, res) => {
      res.json(result);
    },
    res
  );
};

const uploadMsg = (req, res) => {
  res.send('{"status": "Insert OK"}');
};

export {
  upload,
  fileValidation,
  thumbnailResize,
  mediumResize,
  uploadMsg,
  insert2DB,
  selectMedia,
};
