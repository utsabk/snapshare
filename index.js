'use strict';

import dotenv from 'dotenv'
dotenv.config();
import express from 'express';
import bodyParser from 'body-parser';
const app = express();

import mediaRoutes from './routes/media.js';
import userRoutes  from './routes/user.js';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.use('/media', mediaRoutes);
app.use('/user', userRoutes);


app.listen(process.env.APP_PORT, () => {
  console.log(`App running on port ${process.env.APP_PORT}`);
});
