
# ![SnapShare](logo.png)

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

# Overview
SnapShare is an applicaton that allows users to snap pictures and share them up with friends insatntly.
On the other hand it also allows to view posts uploaded by friends and react to them.

# App features
* Snap a picture and upload it quickly.
* Also share short videos or status with friends.
* View all the posts by friends.



