'use strict';
import mysql from 'mysql2';

// create the connection to database
const connect = () => {
  const connection = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
  });

  return connection;
};

const selectUser = (connection, callback, res) => {
  connection.query('SELECT * FROM users', (err, results, fields) => {
    console.log('this is a result from select:-', results); // results contains rows returned by server
    //  console.log(fields); // fields contains extra meta data about results, if available
    console.log('Error while selecting from DB:-', err);
    callback(results, res);
  });
};

const insertUser = (data, connection, callback, res) => {
  connection.query(
    'INSERT INTO users (username, email, password) VALUES(?, ?, ?);',
    data,
    (err, results, fields) => {
      // console.log('this is a result from insertUser:-', results); // results contains rows returned by server
      // console.log(fields); // fields contains extra meta data about results, if available
      console.log('Error while inserting user data to DB:-', err);
      callback(results, res);
    }
  );
};

const insertMedia = (data, connection, callback) => {
  connection.query(
    'INSERT INTO media (category, title, details, thumbnail, medium, original, userID) VALUES (?, ?, ?, ?, ?, ?, ?);',
    data,
    (err, results, fields) => {
      //  console.log('this is a result from insertMedia:-', results);
      console.log('Error while inserting media data to DB:-', err);
      callback();
    }
  );
};

const selectMedia = (connection, callback, res) => {
  connection.query('SELECT * FROM media', (err, results, fields) => {
    console.log('Error while selecting media data :-', err);
    callback(results, res);
  });
};

export {
  connect,
  selectUser,
  insertUser,
  insertMedia,
  selectMedia,
};
