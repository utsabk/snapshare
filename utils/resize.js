'use strict';
import sharp from 'sharp';

const resize =  async (file, size, newPath) => {
  try {
    return await sharp(file)
      .resize(size)
      .toFile(newPath)
  } catch (err) {
    console.log('Error while resizing image:-', err);
  }
};

export default resize;